import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './components/signup/signup.component';
import { LandingComponent } from './components/landing/landing.component';
import { ThankYouComponent } from './components/thank-you/thank-you.component';

const routes: Routes = [
  
  {
    path: 'landing',
    component: LandingComponent,
  },
  {
    path: 'signup',
    component: SignupComponent,
  },
  {
    path: 'thankYou',
    component: ThankYouComponent,
  },
 
  {
    path: '**',
    redirectTo:'landing',
    // component: LandingComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
