import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormArray, Validators } from "@angular/forms";
import { ApiService } from '../../service/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  form: FormGroup;
  constructor(private formBuilder: FormBuilder, private api: ApiService,private router:Router) {
    this.form = this.formBuilder.group({
      Name: ['Deepika', Validators.compose([Validators.required])],
      Email: ['eve.holt@reqres.in', Validators.compose([Validators.required])],
      Password: ['', Validators.compose([Validators.required])],
    });
  }

  ngOnInit() {
  }

  validateAllFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFields(control);
      }
    });
  }



  submitForm() {
    //call api for submit sign up 
    //general API from online
    //den routerLink to thankYou Page


    let obj = {
      name: this.form.value.Name,
      email: this.form.value.Email,
      password: this.form.value.Password
    };
    console.log(obj,"reqObj");
    this.api.post('https://reqres.in/api/register', obj)
      .subscribe((res) => {
        //console.log(res);
        //as using dummy api from "reqres" we are getting response
        // as id and token so currently handle on "id" basis
        if (res['id'] == '4') {
          console.log(res,"Rsponse");
          this.router.navigate(['/thankYou']);
        }
      }, (error) => {
        console.log(error);
      });
  }
}
