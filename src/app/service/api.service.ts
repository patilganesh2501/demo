import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {

  }
  get(endpoint: string, params?: any, reqOpts?: any) {
    return this.http.get(endpoint, reqOpts);
  }
  post(endpoint: string, body?: any, reqOpts: any = {}, auth_header: string = null) {
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    reqOpts.headers = headers;
    return this.http.post(endpoint, body, reqOpts);
  }
  postImage(endpoint: string, body?: any, reqOpts: any = {}, auth_header: string = null) {
    // let headers = new HttpHeaders().set('Content-Type', 'application/json');
    // reqOpts.headers = headers;
    return this.http.post(endpoint, body, reqOpts);
  }

  convertUSDToINR() {
    return this.http.get('https://api.exchangeratesapi.io/latest?base=USD')
    // .map((res:Response) => res.json())
    // .subscribe(data => { 
    //   this.ipAddressId= data.ip; 
    //   console.log("this.ip",this.ipAddressId);
    // })
  }

  convertEURToINR() {
    return this.http.get('https://api.exchangeratesapi.io/latest?base=EUR')
  }

  checkRouteUrl(moduleID) {
    console.log("moduleID", moduleID);
    if (JSON.parse(sessionStorage.getItem('token'))) {

      let moduleList = JSON.parse(sessionStorage.getItem('token')).ModuleLlist;
      for (let i = 0; i < moduleList.length; i++) {
        if (moduleList[i].ModuleID == moduleID) {
          if (moduleList[i].CanRead == 1) {
            console.log("return true;");
            return true;
          } else {
            console.log("return false;");
            return false;
          }

        }

      }
    }

  }
}  
